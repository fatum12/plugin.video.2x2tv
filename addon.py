# -*- coding: utf-8 -*-
import urllib.request, urllib.error, urllib.parse
from urllib.parse import urlencode
import json

import xbmc
import xbmcgui

STREAM_ID = 'dcab9b90a33239837c0f71682d6606da'

headers = {
    'Referer': 'https://uma.media/play/embed/' + STREAM_ID,
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36',
}


def get_playlist_url():
    params = {
        'format': 'json',
        'referer': 'https://online.2x2tv.ru/',
    }
    url = "".join(['https://uma.media/api/play/options/', STREAM_ID, '/?', urlencode(params)])

    response = download(url)
    data = json.loads(response)

    return data['live_streams']['hls'][0]['url']


def download(url):
    req = urllib.request.Request(url, headers=headers)
    return urllib.request.urlopen(req, timeout=30).read()


def debug(msg):
    xbmc.log(msg, xbmc.LOGDEBUG)


def play():
    url = get_playlist_url()
    debug('playlist url: ' + url)
    url += '|' + urlencode(headers)

    item = xbmcgui.ListItem('2x2')

    xbmc.Player().play(url, item)


if __name__ == '__main__':
    play()
